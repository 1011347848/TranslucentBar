## TranslucentBar

#### 透明状态栏
![效果](http://upload-images.jianshu.io/upload_images/912181-965fab182827a0e4.jpg?imageMogr2/auto-orient/strip)

#### 变色状态栏
![效果](http://upload-images.jianshu.io/upload_images/912181-947b33e08c877146.jpg?imageMogr2/auto-orient/strip)

详情可参考：[Translucent System Bar 的最佳实践](http://www.jianshu.com/p/0acc12c29c1b)
package com.weicong.translucentbar;

public class ColorTranslucentBarActivity extends TranslucentBarBaseActivity {

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_color_translucent_bar;
    }
}

package com.weicong.translucentbar;

public class ImageTranslucentBarActivity extends TranslucentBarBaseActivity {

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_image_translucent_bar;
    }
}

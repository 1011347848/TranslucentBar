package com.weicong.translucentbar;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onImage(View view) {
        Intent intent = new Intent(this, ImageTranslucentBarActivity.class);
        startActivity(intent);
    }

    public void onColor(View view) {
        Intent intent = new Intent(this, ColorTranslucentBarActivity.class);
        startActivity(intent);
    }

}
